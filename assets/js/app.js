$(document).ready(function() {

    $("button#btn-new-contact").on("click", function(event){
        event.preventDefault();
        var firstName = $("#name").val();
        var lastName = $("#surname").val();
        var email = $("#email").val();

        $.ajax('http://localhost:3000/contact', {
            method: 'POST',
            data: { 
                firstName: firstName, 
                lastName: lastName, 
                email: email 
            }
        })
        .done(function(response){
            //La richiesta è andata a buon fine
            // console.log("RESP:", response);
            var message = "<div class=\"alert alert-success\" role=\"alert\">"+
                "Il contatto " + response.firstName + " è stato salvato"
            "</div>";
            $("#message").html(message);

            //Prendo la lista dei contatti aggiornata
            $.ajax('http://localhost:3000/contact')
            .done(function(contacts){
                for(var i=0; i<contacts.length; i++) {
                    console.log(contacts[i].firstName);
                }
            })
        })
        .fail(function(err){
            //La richiesta NON è andata a buon fine
            var message = "<div class=\"alert alert-danger\" role=\"alert\">"+
                "Il contatto " + firstName + " <strong>NON</strong> è stato salvato"
            "</div>";
            console.log(err);
            $("#message").html(message);
        });
    });

});